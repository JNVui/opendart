package openwide.opendart;

import java.util.ArrayList;

/**
 * Created by jjacquot on 28/06/16.
 */
public class Player {

    private String mName; //name of player
    private int mSelectingTeamScore; //score player done at team Selection
    private String mTeamName; //name of the player team
    private String mLastScore; //Last score done by player

    private ArrayList<Integer> mListScore = new ArrayList<>(); //Score done by player


    public Player(String name){
        mName = name;
    }


    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmSelectingTeamScore() {
        return mSelectingTeamScore;
    }

    public void setmSelectingTeamScore(int mSelectingTeamScore) {
        this.mSelectingTeamScore = mSelectingTeamScore;
    }

    public String getmTeamName() {
        return mTeamName;
    }

    public void setmTeamName(String mTeamName) {
        this.mTeamName = mTeamName;
    }

    public String getmLastScore() {
        return mLastScore;
    }

    public void setmLastScore(String mLastScore) {
        this.mLastScore = mLastScore;
    }

    public ArrayList<Integer> getmListScore() {
        return mListScore;
    }

    public void setmListScore(ArrayList<Integer> mListScore) {
        this.mListScore = mListScore;
    }

}
