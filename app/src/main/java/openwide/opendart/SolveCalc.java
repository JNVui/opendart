package openwide.opendart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jips on 2/10/16.
 */
public class SolveCalc{

    final static char plus = '+';
    final static char min = '-';
    final static char mul = '*';
    final static char div = '/';

    static String SolveAddMinFromString(String calc){
        String value="0";
        String numberOneBeforeConvert="";
        Boolean isFirst = true;
        Boolean isLastCharASymbol = false;

        int sizeCalc = calc.length();
        List<Float> numberList = new ArrayList<Float>();

        for (int i=0; i<sizeCalc; i++){

            switch (calc.charAt(i)){
                case plus:
                    if(!isLastCharASymbol){
                        isLastCharASymbol=true;
                    } else {
                        break;
                    }
                    if(!isFirst){
                        numberList.add(Float.parseFloat(SolveDivMulFromString(numberOneBeforeConvert)));
                        numberOneBeforeConvert="";
                    }
                    break;
                case min:
                    if(!isLastCharASymbol){
                        isLastCharASymbol=true;
                    } else {
                        break;
                    }
                    if(!isFirst){
                        numberList.add(Float.parseFloat(SolveDivMulFromString(numberOneBeforeConvert)));
                        numberOneBeforeConvert="-";
                    } else {
                        numberOneBeforeConvert="-";
                    }
                    break;
                default:
                    if(isFirst){
                        isFirst=false;
                    }
                    isLastCharASymbol=false;
                    numberOneBeforeConvert = numberOneBeforeConvert+calc.charAt(i);
                    if(i==sizeCalc-1){
                        numberList.add(Float.parseFloat(SolveDivMulFromString(numberOneBeforeConvert)));
                    }
                    break;

            }

        }

        Float result = 0.0f;

        for(Float i:numberList){
            result = result+i;
        }

        return String.valueOf(result);
    }

static boolean CheckValue(char value){

    if(     value == '*' ||
            value == '/' ||
            value == '-' ||
            value == '+' ||
            value == '0' ||
            value == '1' ||
            value == '2' ||
            value == '3' ||
            value == '4' ||
            value == '5' ||
            value == '6' ||
            value == '7' ||
            value == '8' ||
            value == '9' ){

        return true;

    }
    return false;
}

    static String SolveDivMulFromString(String calc){
        String value="0";
        String numberOneBeforeConvert="";
        Boolean isFirst = true;
        Boolean isDivision = false;
        Boolean isLastCharASymbol= false;

        int sizeCalc = calc.length();
        List<Float> numberList = new ArrayList<Float>();

        for (int i=0; i<sizeCalc; i++){

            if(CheckValue(calc.charAt(i))){

                switch (calc.charAt(i)){
                    case mul:
                        if(!isLastCharASymbol){
                            isLastCharASymbol=true;
                        } else {
                            break;
                        }
                        if(isDivision){
                            numberList.add(1/Float.parseFloat(numberOneBeforeConvert));
                            numberOneBeforeConvert="";
                            isDivision = false;
                        }else {
                            numberList.add(Float.parseFloat(numberOneBeforeConvert));
                            numberOneBeforeConvert="";
                        }
                        break;
                    case div:
                        if(!isLastCharASymbol){
                            isLastCharASymbol=true;
                        } else {
                            break;
                        }
                        if(!isFirst){
                            if(isDivision){
                                numberList.add(1/Float.parseFloat(numberOneBeforeConvert));
                                numberOneBeforeConvert="";
                                isDivision = false;
                            }else {
                                numberList.add(Float.parseFloat(numberOneBeforeConvert));
                                numberOneBeforeConvert="";
                            }
                            isDivision = true;
                        }
                        break;
                    default:
                        if(isFirst){
                            isFirst=false;
                        }
                        isLastCharASymbol=false;
                        numberOneBeforeConvert = numberOneBeforeConvert+calc.charAt(i);
                        if(i==sizeCalc-1){
                            if(isDivision){
                                numberList.add(1/Float.parseFloat(numberOneBeforeConvert));
                            }else {
                                numberList.add(Float.parseFloat(numberOneBeforeConvert));
                            }
                        }
                        break;
                }
            }


        }

        Float result = 1.0f;

        for(Float i:numberList){
            result = result*i;
        }

        return String.valueOf(result);
    }

}
