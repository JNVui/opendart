package openwide.opendart;

import java.util.ArrayList;

/**
 * Created by jjacquot on 28/06/16.
 */
public class Team {

    private String mTeamName;
    private int mNumberOfPlayer;
    private ArrayList<Player> mPlayerArraysList = new ArrayList<>();
    private ArrayList<Team> mTeamsNames = new ArrayList<>();
    public Team(ArrayList<Player> playerArrayList){
        mPlayerArraysList = playerArrayList;
    }


    public ArrayList<Player> getmPlayerArraysList() {
        return mPlayerArraysList;
    }

    public void setmPlayerArraysList(ArrayList<Player> mPlayerArraysList) {
        this.mPlayerArraysList = mPlayerArraysList;
    }
    public String getmTeamName() {
        return mTeamName;
    }

    public void setmTeamName(String mTeamName) {
        this.mTeamName = mTeamName;
    }

    public int getmNumberOfPlayer() {
        return mNumberOfPlayer;
    }

    public void setmNumberOfPlayer(int mNumberOfPlayer) {
        this.mNumberOfPlayer = mNumberOfPlayer;
    }

    public ArrayList<Team> getmTeamsNames() {
        return mTeamsNames;
    }

    public void setmTeamsNames(ArrayList<Team> mTeamsNames) {
        this.mTeamsNames = mTeamsNames;
    }

}
