package openwide.opendart;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Game401 extends AppCompatActivity {

    Button mButtonValide;
    EditText mEditTextScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game401);

        mButtonValide = (Button) findViewById(R.id.button_valid_score);
        mButtonValide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),mButtonValide.getText(),Toast.LENGTH_SHORT).show();
            }
        });

        mEditTextScore = (EditText) findViewById(R.id.edittext_score_player);
        mEditTextScore.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                  int after) {
                    }
                    @Override
                    public void onTextChanged(final CharSequence s, int start, int before,
                                              int count) {
                    }
                    @Override
                    public void afterTextChanged(final Editable s) {
                        mButtonValide.setText(SolveCalc.SolveAddMinFromString(mEditTextScore.getText().toString()));
                    }
                });



    }
}
