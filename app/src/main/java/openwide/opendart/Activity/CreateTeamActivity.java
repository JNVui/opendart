package openwide.opendart.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import openwide.opendart.Player;

import openwide.opendart.Game401;
import openwide.opendart.R;

public class CreateTeamActivity extends AppCompatActivity {

    Button mButtonCreateTeams;
    Button mButtonValidScore;
    EditText mEditTextPlayerName;
    EditText mEditTextPlayerScore;

    private ArrayList<Player> players = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);

        mButtonCreateTeams   = (Button)   findViewById(R.id.button_create_teams);
        mButtonValidScore    = (Button)   findViewById(R.id.button_valid_score);
        mEditTextPlayerName  = (EditText) findViewById(R.id.edittext_name_player);
        mEditTextPlayerScore = (EditText) findViewById(R.id.edittext_score_player);

        mButtonValidScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player newPlayer = new Player(mEditTextPlayerName.getText().toString());
                try {
                    newPlayer.setmLastScore(mEditTextPlayerScore.getText().toString());
                } catch (NumberFormatException e) {

                }
                players.add(newPlayer);
            }
        });

        mButtonCreateTeams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create teams

            }
        });
    }
}
