package openwide.opendart.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import openwide.opendart.Game401;
import openwide.opendart.R;

public class MainActivity extends AppCompatActivity {

    Button mButton401;
    Button mButton123;
    Button mButtonKiller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButton123 = (Button) findViewById(R.id.button_123);
        mButton401 = (Button) findViewById(R.id.button_401) ;
        mButtonKiller = (Button) findViewById(R.id.button_killer);

        mButton401.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Game401.class));
            }
        });

        mButtonKiller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Game401.class));
            }
        });
    }
}
